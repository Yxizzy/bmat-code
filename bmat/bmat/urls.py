from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('api/', include(('metadata.urls', "app_name"), namespace="metadata")),
    path('admin/', admin.site.urls)
]
