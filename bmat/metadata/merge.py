import itertools
"""
    This function is written to analyse the works_metadata.csv,
    get rid of duplicate values
"""


def keyfunc(x):
    return x['iswc']


def unique_list(l):
    ulist = []
    [ulist.append(x) for x in l if x not in ulist]
    return ulist


def read_file():
    with open('./bmat/metadata/works_metadata.csv') as file:
        file_data = file.read()

        return reconcile_data(file_data)


def reconcile_data(file_data):
    lines = file_data.split("\n")

    # loop over the lines and add to list
    list_of_dict = []
    for line in lines:
        try:
            fields = line.split(",")
            data_dict = {}
            data_dict["title"] = fields[0]
            data_dict["contributors"] = fields[1]
            data_dict["iswc"] = fields[2]
            data_dict["source"] = fields[3]
            list_of_dict.append(data_dict)
        except Exception as e:
            print(e)
    final_data = []

    for iswc, group in itertools.groupby(list_of_dict, keyfunc):
        new_group = list(group)
        concat_value = {
            k: [d.get(k) for d in new_group]
            for k in set().union(*new_group)
        }

        # clean up title
        concat_value['title'] = ' '.join(concat_value['title'])
        concat_value['title'] = ' '.join(unique_list(
            concat_value['title'].split()))

        # clean up contributor
        concat_value['contributors'] = ' '.join(concat_value['contributors'])
        concat_value['contributors'] = ' '.join(unique_list(
            concat_value['contributors'].split()))

        # clean up sources
        concat_value['source'] = ' '.join(concat_value['source'])
        concat_value['source'] = ' '.join(unique_list(
            concat_value['source'].split()))

        if iswc != '':
            final_data.append(
                {'iswc': iswc, 'title': concat_value['title'],
                    'contributors': concat_value['contributors'],
                    'source': concat_value['source']})
    del final_data[0]
    return(final_data)

read_file()
