from django.db import models


class Data(models.Model):
    """
    Data Model
    Defines the attributes of a music workdata
    """
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=30)
    contributors = models.CharField(max_length=500)
    iswc = models.CharField(max_length=20)
    source = models.CharField(max_length=300)

    def get_data_by_iswc(self):
        return '%s' % (self.title)

    def __str__(self):
        return '%s, %s, %s, %s' % (
            self.title, self.contributors,
            self.iswc, self.source)
