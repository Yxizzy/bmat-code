from django.test import TestCase, Client
from metadata.merge import read_file
from metadata.models import Data
from django.urls import reverse
from .serializers import DataSerializer
from rest_framework import status

client = Client()


class DataTestCase(TestCase):
    """
    This testcase is to ascertain if data in works_metadata.csv was reconciled
    """
    def test_confirm_reconcilation(self):
        self.assertEqual(len(read_file()), 4)


class ApiTestCase(TestCase):
    """
    This testcase is to confirm the created APIs are working
    """
    def test_upload(self):
        file_data = open('./bmat/metadata/works_metadata.csv')
        response = client.post(
            reverse('metadata:data-list'),
            {'files': file_data})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_data(self):
        response = client.get(reverse('metadata:data-list'))
        data = Data.objects.all()
        serializer = DataSerializer(data, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_data_by_iswc(self):
        response = client.get(
            reverse(
                'metadata:single-data-list',
                kwargs={'iswc': "T9214745718"}))
        data = Data.objects.filter(iswc="T9214745718").distinct()
        serializer = DataSerializer(data, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_file_export(self):
        response = client.get(reverse('metadata:export-data-list'))
        self.assertEqual(response.data, "csv generated")
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DataModelTest(TestCase):
    """ Test module for Data model """

    def setUp(self):
        Data.objects.create(
            title='Knocks you down', contributors="Chris Brown",
            iswc='T1237884933', source='Black')
        Data.objects.create(
            title='Get up', contributors="Davido",
            iswc='T4346748333', source='Black')

    def test_puppy_breed(self):
        data_chris = Data.objects.get(iswc='T1237884933')
        data_david = Data.objects.get(iswc='T4346748333')
        self.assertEqual(data_chris.get_data_by_iswc(), "Knocks you down")
        self.assertEqual(
            data_david.get_data_by_iswc(), "Get up")
