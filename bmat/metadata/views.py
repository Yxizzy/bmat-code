import os
from rest_framework import viewsets
from .models import Data
from .serializers import DataSerializer
from rest_framework.response import Response
from sqlalchemy import create_engine
import pandas
from .merge import reconcile_data


class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer

    def create(self, request):
        file = request.FILES['files']
        # Check file type
        if not file.name.endswith('.csv'):
            return Response('File is not CSV type')

        # If file is too large, return
        if file.multiple_chunks():
            return Response("Uploaded file is too big (%.2f MB)." % (
                            csv_file.size/(1000*1000),))
        file_data = file.read().decode("utf-8")
        value = reconcile_data(file_data)

        # save reconciled data to the database
        for i in range(len(value)):
            Data.objects.update_or_create(iswc=value[i]['iswc'], defaults={
                'title': value[i]['title'],
                'contributors': value[i]['contributors'],
                'source': value[i]['source']
            })

        return Response(value)


class SingleDataViewSet(viewsets.ModelViewSet):
    """
    this function filters data by iswc and returns response in json
    """
    def list(self, request, iswc):
        queryset = Data.objects.filter(iswc=iswc).distinct()
        serializers = DataSerializer(queryset, many=True)
        return Response(serializers.data)


class ExportDataViewSet(viewsets.ModelViewSet):

    """
    This function generates csv from the models and
    returns the link to download file
    """
    def list(self, request):
        conn = create_engine('postgresql://hello_django:hello_django@db:5432/hello_django_dev')
        query = str(Data.objects.all().query)
        # generate csv file using pandas
        results = pandas.read_sql_query(query, conn)
        results.to_csv("./metadata.csv", index=False)

        return Response("csv generated")
